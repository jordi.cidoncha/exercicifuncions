import java.util.Scanner

fun writeALine(spaces: Int, numOfChars: Int, character: Char): String {
    var result = ""
    repeat(spaces){ result += " " }
    repeat(numOfChars){ result += character}
    return result
}

fun main() {
    println("Introdueix un nombre d'espais, una repetició i un caràcter ")
    val scanner = Scanner(System.`in`)
    println("Línia: ${writeALine(scanner.nextInt(), scanner.nextInt(), scanner.next().single())}")
}
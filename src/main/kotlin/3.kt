import java.util.Scanner

fun containsCharacter(s: String, pos: Int): String {
    if (pos > s.lastIndex) return "La mida de l'String és inferior $pos"
    return s[pos].toString()
}

fun main() {
    println("Introdueix un String i una posició ")
    val scanner = Scanner(System.`in`)
    println("El caràcter de la posició és: ${containsCharacter(scanner.nextLine(), scanner.nextInt())}")
}
import java.util.Scanner

fun powTheNum(num1: Int, num2: Int): Int {
    var result = 1
    for (i in 1..num2) result *= num1
    return result
}

fun main() {
    println("Introdueix un número i quantes vegades el vols elevar ")
    val scanner = Scanner(System.`in`)
    println("El resultat és: ${powTheNum(scanner.nextInt(), scanner.nextInt())}")
}
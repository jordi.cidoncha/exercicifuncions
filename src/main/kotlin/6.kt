import java.util.Scanner

fun stringSplitter(s: String, c: Char): MutableList<String> {
    val result = mutableListOf<String>()
    var subsequence = ""
    for (character in s) {
        if (character == c) {
            result.add(subsequence)
            subsequence = ""
        } else subsequence += character
    }
    result.add(subsequence)
    return result
}

fun main() {
    println("Introdueix un String i un caràcter separador ")
    val scanner = Scanner(System.`in`)
    println("Llista d'strings: ${stringSplitter(scanner.nextLine(), scanner.next().single())}")
}
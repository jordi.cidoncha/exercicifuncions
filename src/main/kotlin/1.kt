import java.util.Scanner

fun stringLength(s: String): Int {
    var result = 0
    for (character in s) result++
    return result
}

fun main() {
    println("Introdueix un String ")
    val scanner = Scanner(System.`in`)
    println("Nombre de caràcters: ${stringLength(scanner.nextLine())}")
}
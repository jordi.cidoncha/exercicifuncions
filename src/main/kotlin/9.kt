import java.util.Scanner

fun paintACross(size: Int, c: Char) {
    for (i in 0 until size) {
        if (i == size/2) repeat(size) { print(c) }
        else {
            repeat(size/2) { print(" ") }
            print(c)
            repeat(size/2) { print(" ") }
        }
        println()
    }
}

fun main() {
    println("Introdueix una mida i un caràcter per pintar una creu ")
    val scanner = Scanner(System.`in`)
    paintACross(scanner.nextInt(), scanner.next().single())
}
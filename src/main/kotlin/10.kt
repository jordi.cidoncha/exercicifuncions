import java.util.Scanner

fun frameAString(s: String, c: Char) {
    repeat(s.length+4) { print(c) }
    println()
    print(c)
    repeat(s.length+2) { print(" ") }
    println(c)
    println("$c $s $c")
    print(c)
    repeat(s.length+2) { print(" ") }
    println(c)
    repeat(s.length+4) { print(c) }
    println()
}

fun main() {
    println("Introdueix un string i un caràcter per pintar un marc al seu voltant ")
    val scanner = Scanner(System.`in`)
    frameAString(scanner.nextLine(), scanner.next().single())
}
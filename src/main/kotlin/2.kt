import java.util.Scanner

fun stringEquality(s1: String, s2: String): Boolean {
    if (s1.length != s2.length) return false
    for (i in s1.indices) {
        if(s1[i] != s2[i]) return false
    }
    return true
}

fun main() {
    println("Introdueix dos Strings ")
    val scanner = Scanner(System.`in`)
    println("Són iguals? ${stringEquality(scanner.nextLine(), scanner.nextLine())}")
}
import java.util.Scanner

fun stringSubsequence(s: String, start: Int, end: Int): String {
    if (start !in 0..s.lastIndex || end !in 0..s.lastIndex || start >= end) return "La subseqüència $start-$end de l'String no existeix"
    var result = ""
    for (i in start until end) {
        result += s[i]
    }
    return result
}

fun main() {
    println("Introdueix un String i dues posició ")
    val scanner = Scanner(System.`in`)
    println("Las subsequència de l'String és: ${stringSubsequence(scanner.nextLine(), scanner.nextInt(), scanner.nextInt())}")
}
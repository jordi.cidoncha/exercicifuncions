import java.util.Scanner

fun stringToCharList(s: String): MutableList<Char> {
    var result = mutableListOf<Char>()
    for (character in s) result.add(character)
    return result
}

fun main() {
    println("Introdueix un String ")
    val scanner = Scanner(System.`in`)
    println("La llista de caràcters de l'string és: ${stringToCharList(scanner.nextLine())}")
}